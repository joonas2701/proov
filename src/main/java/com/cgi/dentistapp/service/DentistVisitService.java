package com.cgi.dentistapp.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

	@Autowired
	private DentistVisitDao dentistVisitDao;

	public void addVisit(String dentistName, Date visitTime, String clocktime) {
		DentistVisitEntity visit = new DentistVisitEntity(dentistName, visitTime, clocktime);
		dentistVisitDao.create(visit);
	}

	public List<DentistVisitEntity> listVisits() {
		return dentistVisitDao.getAllVisits();
	}

	public void editVisit(long id, String dentistName, Date visitTime, String clocktime) {
		DentistVisitEntity visit = dentistVisitDao.find(id);
		if (visit != null){
			dentistVisitDao.remove(visit);
			visit.setDentistName(dentistName);
			visit.setVisitTime(visitTime);
			dentistVisitDao.create(visit);
		}
	}
	
	public DentistVisitEntity findVisit(long id){
		DentistVisitEntity visit = dentistVisitDao.find(id);
		return visit;
	}
	
	

	public void removeVisit(long id) {
		DentistVisitEntity visit = this.findVisit(id);
		if (visit != null) {
			dentistVisitDao.remove(this.findVisit(id));
		}
	}

}
