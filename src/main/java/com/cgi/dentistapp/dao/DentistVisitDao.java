package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }
    
    public void remove(DentistVisitEntity visit) {
    	entityManager.remove(visit);
    }
    
    public DentistVisitEntity find(long id){
    	List<DentistVisitEntity> list = this.getAllVisits();
    	for (DentistVisitEntity visit : list){
    		if (id == visit.getId()){
    			return visit;
    		}
    	}
		return null;
    }
    
    public DentistVisitEntity find(DentistVisitEntity searchVisit){
    	List<DentistVisitEntity> list = this.getAllVisits();
    	for (DentistVisitEntity visit : list){
    		if (searchVisit == visit){
    			return visit;
    		}
    	}
		return null;
    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e").getResultList();
    }
}
