package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;
    
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/")
    public String showMenu(DentistVisitDTO dentistVisitDTO) {
        return "menu";
    }
    
    @GetMapping("/form")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO) {
        return "form";
    }
    
    @RequestMapping(value = "/registered" , method = RequestMethod.GET)
    public String showRegistered(DentistVisitDTO dentistVisitDTO, Model model) {
    	String registered = "";
    	for (DentistVisitEntity visit :dentistVisitService.listVisits()){
    		registered = registered + visit.toString() + ";";
    	}
    	model.addAttribute("registeredPeople",registered);
        return "registered";
    }
    
    
    @PostMapping(params= "kustuta", value = "/registered")
    public String deleteVisit(long id){
    	System.out.println("Tere");
    	return "form";
    	//dentistVisitService.removeVisit(id);
    	//return "form";
    }

    @PostMapping("/")
    public String postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "form";
        }
        dentistVisitService.addVisit(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime(), dentistVisitDTO.getVisitClockTime());
        return "redirect:/results";
    }

}
